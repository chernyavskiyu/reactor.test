var urlTable = undefined;
$(document).ready(function () {
    loadUrlTable();

    $('.create-button').click(function () {
        $.ajax({
            url: '/short-url/create',
            dataType: 'json',
            data: {
                '_token': window._token,
                'url': $('input[name=url]').val(),
                'active_from': $('input[name=active_from]').val(),
                'active_to': $('input[name=active_to]').val(),
            },
            type: 'post',
            success: function (resp) {
                $('.message-block').text(resp.message);
                $('.message-block').addClass('success');
                $('.message-block').removeClass('error');
                $('.message-block').removeClass('hidden');

                urlTable.ajax.reload();
            },
            error: function (resp) {
                $('.message-block').text(resp.responseJSON.message);
                $('.message-block').addClass('error');
                $('.message-block').removeClass('success');
                $('.message-block').removeClass('hidden');
            }
        });
    });
});


function loadUrlTable() {
    urlTable = $('#url_table').DataTable({
        "ajax": {
            "url": '/short-url/list',
            "type": 'POST',
            "data": {
                '_token': window._token
            },
            "dataSrc": ""
        },
        "order": [[0, "desc"]],
        "pageLength": 10,
        "columns": [
            {"data": "id"},
            {"data": "url"},
            {"data": "short_url"},
            {"data": "active_from"},
            {"data": "active_to"},
            {"data": "created"}
        ]
    });
}
