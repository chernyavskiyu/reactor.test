<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('title')
</head>
<body>
    @include('includes.header')

    @yield('content')
</body>
<script type="text/javascript" src="{{ asset('/public/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('/public/js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/public/js/script.js') }}"></script>
<script>
    window._token = '{{ csrf_token() }}';
</script>
<link rel="stylesheet" href="{{ asset('/public/css/datatables.min.css') }}" />
<link rel="stylesheet" href="{{ asset('/public/css/styles.css') }}" />
</html>
