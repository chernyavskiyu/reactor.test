@extends('layouts.default')

@section('title')
    <title>Reactor test app</title>
@endsection

@section('content')
    <div class="container">
        <div>
            <p>
                <a href="https://gitlab.com/chernyavskiyu/reactor.test" target="_blank">GitLab url</a>
            </p>
        </div>
        <div class="create-block">
            <div class="create-block-title">Enter data for create new url</div>
            <div class="message-block hidden"></div>
            <div class="input-block">
                <input type="text" name="url" placeholder="url">
                <input type="date" name="active_from" placeholder="active from">
                <input type="date" name="active_to" placeholder="active to">
            </div>
            <button type="submit" class="create-button">Create</button>
        </div>

        <div class="table-block">
            <table id="url_table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>URL</th>
                    <th>Short url</th>
                    <th>Active from</th>
                    <th>Active to</th>
                    <th>Created</th>
                </tr>
                </thead>
            </table>
        </div>

    </div>
@endsection
