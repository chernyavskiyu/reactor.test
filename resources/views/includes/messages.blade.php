@if($errors->any())
    <div class="notification alert">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('success'))
    <div class="notification success">
        <ul>
            <li>{{ session('success') }}</li>
        </ul>
    </div>
@endif
