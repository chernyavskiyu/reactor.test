<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\ShortUrlModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class ShortUrlController extends Controller
{
    private $short_url_model = null;

    public function __construct()
    {
        $this->short_url_model = new ShortUrlModel();
    }

    public function mainPage()
    {
        return view('home', ['url_list' => $this->getUrlList()]);;
    }

    public function createUrl(Request $request)
    {
        $validation = $request->validate([
            'url' => 'required|min:5|max:256|url',
            'active_to' => 'required|date|after:today'
        ]);
        return $this->short_url_model->createUrl(
            $request->input('url'),
            $request->input('active_to'),
            $request->input('active_from')
        );
    }

    public function getUrlList()
    {
        $to_return = array_map(function ($link) {
            $link['url'] = $this->generateHtmlUrl($link['url']);
            $link['short_url'] = $this->generateHtmlUrl($link['short_url'], true);
            return $link;
        }, $this->short_url_model->getAllUrls());

        return json_encode($to_return);
    }

    public function findUrl($url)
    {
        $url_item = $this->short_url_model->findUrl($url);
        if (!empty($url_item)) {
            $link = $url_item->toArray()['url'];
            return redirect($link);
        } else {
            return abort(404);
        }
    }

    private function generateHtmlUrl($url, $local = false)
    {
        $url = $local
            ? URL::to($url)
            : $url;
        return sprintf('<a href="%s" target="_blank">%s</a>',
            $url, $url
        );
    }
}
