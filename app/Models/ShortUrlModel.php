<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class ShortUrlModel extends Model
{
    use HasFactory;

    protected $table = 'short_url';

    public function createUrl($url, $active_to, $active_from = null)
    {
        $this->url = $url;
        $this->short_url = $this->generateShortUrl();
        if (!empty($active_from)) {
            $this->active_from = $active_from;
        }
        $this->active_to = $active_to;
        $this->timestamps = false;

        $this->save();
        return json_encode(['status' => 'created', 'message' => 'Link was created']);
    }

    public function getAllUrls()
    {
        return ShortUrlModel::all()->toArray();
    }

    public function findUrl($url)
    {
        $date = date('Y-m-d H:i:s');
        return ShortUrlModel::where('short_url', '=', $url)
            ->where('active_to', '>=', $date)
            ->where('active_from', '<=', $date)
            ->first();
    }

    private function generateShortUrl(): string
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMOPQRSTUVWXYZ';
        $shortUrl = substr(str_shuffle($permitted_chars), 0, 5);
        if (!ShortUrlModel::where('short_url', '=', $shortUrl)->first()) {
            return $shortUrl;
        } else {
            return $this->generateShortUrl();
        }
    }
}
