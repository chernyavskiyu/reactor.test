<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShortUrlController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/',
    [
        ShortUrlController::class,
        'mainPage'
    ])->name('home');

Route::get(
    '/{url}',
    [
        ShortUrlController::class,
        'findUrl',
        [
            'url'
        ]
    ]
)->where('url', '[a-zA-Z0-9]{5}');

Route::post(
    '/short-url/list',
    [
        ShortUrlController::class,
        'getUrlList'
    ]
)->name('list');

Route::post(
    '/short-url/create',
    [
        ShortUrlController::class,
        'createUrl'
    ]
)->name('short_url_create');
